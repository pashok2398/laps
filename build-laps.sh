#!/bin/sh

test -z "${PACK_PACKAGE}" && PACK_PACKAGE=laps
test -z "${PACK_VERSION}" && PACK_VERSION="$( cat src/usr/share/doc/laps/version.txt )"
test -z "${PACK_DIR}" && PACK_DIR="$( readlink -f "$( dirname "${0}" )" )"
test -z "${RPMBUILD_SOURCES_DIR}" && RPMBUILD_SOURCES_DIR="/home/${USER}/rpmbuild/SOURCES"

test -z "${PACK_TYPE}" && PACK_TYPE="${1}"

case "${PACK_TYPE}" in
   rpm|"")
      PACK_TYPE=rpm
      :
      ;;
   deb)
      echo "Gotta say unh! Not implemented yet."
      exit 1
      ;;
   *)
      echo "Unknown type ${PACK_TYPE}"
      exit 1
      ;;
esac

case "${PACK_TYPE}" in

   rpm)
      pushd "${PACK_DIR}/.." 1>/dev/null 2>&1
      /bin/rm -f "${RPMBUILD_SOURCES_DIR}/${PACK_PACKAGE}-${PACK_VERSION}.tgz"
      tar -zcf "${RPMBUILD_SOURCES_DIR}/${PACK_PACKAGE}-${PACK_VERSION}.tgz" --exclude="*.conf" --exclude=".git" --exclude=".*.swp" "${PACK_PACKAGE}"
      popd 1>/dev/null 2>&1
      pushd "${PACK_DIR}" 1>/dev/null 2>&1
      rpmbuild -ba "${PACK_PACKAGE}.spec"
      ;;
  
esac
